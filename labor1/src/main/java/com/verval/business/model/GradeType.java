package com.verval.business.model;

public enum GradeType {
    Laboratory,
    Seminar,
    Lecture,
    Exam,
    Granted
}