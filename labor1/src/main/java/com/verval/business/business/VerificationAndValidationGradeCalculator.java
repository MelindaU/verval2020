package com.verval.business.business;

import com.verval.business.exception.InvalidPointException;
import com.verval.business.model.GradeType;
import com.verval.business.model.Point;

import java.util.ArrayList;
import java.util.stream.Stream;

public class VerificationAndValidationGradeCalculator {
    private  ArrayList<Point> points;
    private String studentId;
    private int yearOfStudy;

    public VerificationAndValidationGradeCalculator() {
        points = new ArrayList<Point>();
        points.add(new Point(10,10, GradeType.Granted));
    }

    public ArrayList<Point> getPoints() {
        return points;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public int getYearOfStudy(boolean exceptionStudent) {
        if(exceptionStudent) {
            return Integer.MAX_VALUE;
        }
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public void addPoint(Point point) {
        validatePoint(point);
        points.add(point);
    }

    public double getGrade() {
        return points.stream().mapToDouble(Point::getPoint).sum() / 10;
    }

	public boolean isPassing() {
        if (getPointsSumOfType(GradeType.Laboratory) < 10) {
            return false;
        }

        if (getPointsSumOfType(GradeType.Seminar) < 10) {
            return false;
        }

        if (getPointsSumOfType(GradeType.Lecture) < 10) {
            return false;
        }

        Stream<Point> examPoint = points.stream().filter(point -> point.getType() == GradeType.Exam);
        if (examPoint.count() != 1) {
            return false;
        }

        if ((int) getGrade() < 5) {
            return false;
        }
        return true;
    }

    private void validatePoint(Point point) {
        if (point.getPoint() > 10) {
            throw new InvalidPointException("The point value cannot be higher than 10!");
        }
        if (point.getPoint()  < 0) {
            throw new InvalidPointException("The point value cannot be lower than 0!");
        }
    }

    private double getPointsSumOfType(GradeType type) {
        return points.stream().filter(point->point.getType() == type)
                .mapToDouble(point->point.getPoint()).sum();
    }
}
