# Java Unit Testing with JUnit and Mockito *- II. rész*


## Laborfeladat
> **Laborhoz szükséges:** Java, Maven, IDE *(pl: Eclipse, IntelliJ, egyéb)*
> 
> **JUnit Laborházi második része:**
>  - Függőségek megszüntetése Mockito-val az OrderTest osztályban:
>     - 4 darab unit teszt, a következők használata:
>       - Mock, Spy, Captor annotációk
>       - when().thenReturn(), verify()
>       - Matcherek (pl. anyBoolean())
>       
> **HATÁRIDŐ:** csoportodnak megfelelő **3. labor** a házi első és második része együtt

## Bevezető
### Mockito 
*Mockito is a mocking framework*, Java alapú library, Java alkalmazások tesztelésére. (A háttérben Java Reflectiont használ)

### Inicializálás
Mindenekelőtt a Mockito-s tesztekben használt annotált objektumokat inicializálni kell. Erre az egyik módszer a következő:
```
@BeforeEach
public void init() {
  MockitoAnnotations.initMocks(this);
}
```

### Mock
Egy mock objektum *dummy data-t* térít vissza és elkerül minden külső függőséget. A tesztjeink így egyszerűbbé válnak, és pontosan azt tesztelhetjük amit szeretnénk.

##### Mock, Spy, Captor
* **Mock**: Semmilyen valós metódusa nem fog meghívódni az ezzel annotált objektumnak.
* **Spy**: A valós metódusok lesznek meghívva, az általunk expliciten megadott metódusok lesznek mockolva.
```
@Spy
List<String> spiedList = new ArrayList<String>();
 
@Test
public void whenUseSpyAnnotation_thenSpyIsInjected() {
    // a valódi .add() metódus hívódik meg.
    spiedList.add("one");
    spiedList.add("two");
    
    /* 
    .verify metódus ellenőrzi, hogy megtörtént-e 
    interakció az adott függvénnyel.
    */
    Mockito.verify(spiedList).add("one");
    Mockito.verify(spiedList).add("two");
 
    // a valódi .size() metódus hívódik meg.
    assertEquals(2, spiedList.size());
 
    /* 
    A következőkben a spiedList objektum .size() metódusa
    100-at fog visszatéríteni.
    */
    Mockito.doReturn(100).when(spiedList).size();
    // nem fog meghívódni a valódi .size() metódus
    assertEquals(100, spiedList.size());
}
```

* **Captor**: argumentumok ellenőrzése
```
    @Mock
    List mockedList;

    @Captor
    ArgumentCaptor argCaptor;

    @Test
    public void whenUseCaptorAnnotation_thenTheSam() {
        mockedList.add("one");
        Mockito.verify(mockedList).add(argCaptor.capture());

        assertEquals("one", argCaptor.getValue());
    }
```
##### Matcherek
* A következő utasítás hatására amikor a flowerService .analyze() metódusa a "poppy" argumentummal fog meghívódni, "Flower" lesz visszatérítve.
  ```
  doReturn("Flower").when(flowerService).analyze("poppy");
  ```
* A következő utasítás hatására amikor a flowerService .analyze() metódusa a bármilyen String argumentummal fog meghívódni, "Flower" lesz visszatérítve.
  ```
  when(flowerService.analyze(anyString())).thenReturn("Flower");
  ```
> Ha a függvényünknek több paramétere van, mindegyik paramétert *Matcher-rel* vagy mindegyiket értékkel kell megadnunk. A kettő kombinálását nem támogatja a Mockito.